#!/usr/bin/env bash

set -eo pipefail

if grep -q 'NAME="Alpine Linux"' /etc/*-release
then
    apk update > /dev/null
    UPDATES=$(($(apk version -l '<' | wc -l)-1))
fi

if grep -q 'NAME="Debian GNU/Linux"' /etc/*-release
then
    apt-get update > /dev/null
    UPDATES=$(apt-get -u upgrade --dry-run | grep -Eo '[0-9]+ upgraded,' | grep -Eo '[0-9]+')
fi

if grep -q 'NAME="openSUSE Leap"' /etc/*-release
then
    UPDATES=$(zypper -q patch-check | grep -Eo '[0-9]+ parches necesarios' | grep -Eo '[0-9]+')
fi

if grep -q 'NAME="CentOS Linux"' /etc/*-release
then
    OUTPUT_LINES=$(yum --quiet check-update | wc -l)
    if [ "$OUTPUT_LINES" -eq 0 ]
    then
        UPDATES="$OUTPUT_LINES"
    else
        UPDATES="$((OUTPUT_LINES-1))"
    fi
fi

echo -n "$UPDATES"

exit 0
