#!/usr/bin/env bash

set -eo pipefail

for service in $(cat "$@" | jq -c '.[]')
do
    SERVICE=$(echo "$service" | jq '. | .service' | cut -d '"' -f 2)
    COMPOSE_PATH=$(echo "$service" | jq '. | .path' | cut -d '"' -f 2)
    RESTART=$(echo "$service" | jq '. | .restart' | cut -d '"' -f 2)
    PULL_ONLY=$(echo "$service" | jq '. | .pull_only' | cut -d '"' -f 2)
 
    if [ "${COMPOSE_PATH:(-1)}" == '/' ]
    then
        COMPOSE_PATH=$COMPOSE_PATH'docker-compose.yml'
    fi
    echo "Processing service $SERVICE from $COMPOSE_PATH"
    
    python -m compose -f "$COMPOSE_PATH" build --pull "$SERVICE"

    if [ "$PULL_ONLY" == 'false' ]
    then
        UPDATES=$(python -m compose -f "$COMPOSE_PATH" run --rm  --user=0 --entrypoint "/updates_checker.sh" "$SERVICE")
        echo "There are $UPDATES update(s) to apply"
        if [ "$UPDATES" -gt 0 ]
        then
            python -m compose -f "$COMPOSE_PATH" build --no-cache "$SERVICE"
        fi
    fi
    
    if [ "$RESTART" == 'true' ]
    then
        python -m compose -f "$COMPOSE_PATH" up -d "$SERVICE"
    fi
done

exit 0
