# A simple tool to keep doker-compose services updated

## How does it work?

This repo is made of two parts:

- The `updates_checker.sh` script, that needs to be inisde the image of the services you want to keep up to date.
- And `update_services.sh`, which will run that script inside the sevices you desire, and update them if necesary.

To make the magic happen, you need to do three things:

### Add the `updates_checker.sh` script to the image of the service you want to update

You can do that by downloading [the script from here](https://bitbucket.org/fernandocollova/docker_upgrade/raw/master/updates_checker.sh) to your Dockerfile, and making it executable.

Also, you should check it's siganture to make sure you're running what you expect to be running.

You can download the signature [from here](https://bitbucket.org/fernandocollova/docker_upgrade/raw/master/updates_checker.sig) and get my public key with `gpg2 --keyserver hkp://pool.sks-keyservers.nethttp://pool.sks-keyservers.net/ --recv-keys 7A5F664591740BFE9A746DD7BE3EC67B57EEAE3F` or `curl "https://fernandocollova.com/personal_key.asc" | gpg2 --import`

### Tell the `updates_checker.sh` script where to find the services it needs to keep updated

The services need to run on debian or alpine based images, and be described in a json file in this way:

```json
[
    {
        "path": "PATH_TO_COMPOSE_FILE",
        "service": "SERVICE_NAME"
    },{
        "path": "PATH_TO_COMPOSE_FILE",
        "service": "SERVICE_NAME"
    },
]
```

Where the `PATH_TO_COMPOSE_FILE` is either a path ending in `/` where you can find a `docker-compose.yml` file, or a path ending with a file name, in case you don't name your comose files the default way.

### Run and profit

Now you just need to call `update_services.sh` with the path to that json file as it's sole argument, and that's it! Your services will be updated
